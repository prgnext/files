// +build linux freebsd openbsd darwin

package files

import (
	"fmt"
)

func (d *Envofile) vSocket() string {
	return fmt.Sprintf("%s:/var/run/docker.sock", d.env.HostSocket())
}
