package files

import (
	"fmt"
)

func (d *Envofile) vSocket() string {
	return fmt.Sprintf("%s://./pipe/docker_engine", d.env.HostSocket())
}
